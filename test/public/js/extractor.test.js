import { extractReportersData } from "../../../public/js/extractor.js";

describe('Reporters Test', function () {

    var testData;

    beforeEach(() => {
        testData = {
            total: 100,
            issues: [{
                fields: {
                    reporter: {
                        key: "key1",
                        displayName: "Key 1"
                    }
                }
            }, {
                fields: {
                    reporter: {
                        key: "key2",
                        displayName: "Key 2"
                    }
                }
            }, {
                fields: {
                    reporter: {
                        key: "key2",
                        displayName: "Key 2"
                    }
                }
            }]
        }
    })

    it('should return reporters list with requestsCount', function () {
        chai.expect(extractReportersData(testData)).to.deep.equal((
            [{ name: 'Key 2', requestCount: 2 },
            { name: 'Key 1', requestCount: 1 }]));
    });

});