# Atlassian JSD Add-on - Top Reporters

## Jira Service Desk Add-on to Show Top Reporters with total number of requests

Project is built on [Atlassian Add-on using Express Template](https://bitbucket.org/atlassianlabs/atlassian-connect-jira-example/src/master/) 

It uses JSD Rest API to get all the issues of the current project and group by reporters and show Reporter Name along with Requests Count on the UI in descending order

![Image](/hasan-jsd-project_-_Jira.jpg)

Main Plugin code is in [public/js]('/public/js/addon.js')

# Installation & Run

create `credentials.json` at the root after checking out as per the following template

```
{
    "hosts": {
        "{
          {your-account-name}.atlassian.net": {
            "product": "jira",
            "username": "",
            "password": ""
        }
    }
}
```
add Atlassian account name and user (email address) and password.

Run npm as
`npm install`
`npm start`

Above command will build the add-on and automatically register it using credentials. Go to JSD and check the add-on under the project with the name `Top Reporters`.

# Screencast
[Video](/screencast.mp4)


# Improvements
There's always room for improvements but following should be highly recommended,

- Strong Type Definitions is a must for any Javascript Project such as TypeScript!
- Compiler/Bundler like Babel/Webpack can be used to enable modern JS features and bundle things nicely.
- Handlbars Dynamic Partials can be used to decouple UI fragments from service code.


# Testing
A simple test added using *Mocha* and *Chai* to test front-end extractor functionality.
See `test/public/test.html`