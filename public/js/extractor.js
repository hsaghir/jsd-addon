
  /**
   * 
   * Calculate Total Number of Requsts grouped by reporters and sort in descending order
   Example:
      [{
        name: 'Name1',
        requestCount: 4 
      },{
        name: 'Name2',
        requestCount: 2 
      }]
  * @param {*} responseJson 
  */
  const extractReportersData = (responseJson) => {
    var reporters = [];
    responseJson.issues.forEach(issue => {
      const userKey = issue.fields.reporter.key;
      const reporter = reporters[userKey];
      if (!!reporter) {
        reporter.requestCount = reporter.requestCount + 1;
      } else {
        reporters[userKey] = {
          name: issue.fields.reporter.displayName,
          requestCount: 1
        }
      }
    })
    return Object.values(reporters).sort(compareRequestsCount);
  }

/**
 * Simple Comparison Function with requests counts in descending order
 * @param {*} a 
 * @param {*} b 
 */
const compareRequestsCount = (a, b) => {
  if (b.requestCount < a.requestCount)
    return -1;
  if (b.requestCount > a.requestCount)
    return 1;
  return 0;
}

export {extractReportersData}

