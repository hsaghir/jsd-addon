import { extractReportersData } from "./extractor.js";

/**
 * Add-on to Find Top Reporters with Request Counts
 */
$(document).ready(function () {

  // Serach for Project Issues
  AP.request({
    url: `/rest/api/3/search`,
    success: function (response) {
      // Parse the response JSON
      var responseJson = JSON.parse(response);

      // Did we get any requests back?
      if (responseJson.total > 0) {
        // Create a table with the resulting requests
        const reportersIssuesList = extractReportersData(responseJson);
        createReporterTable(reportersIssuesList);

      } else {
        // Show a link to the Customer Portal
        createNoIsssuesFound()
      }
    },
    error: function (err) {
      createNoIsssuesFound(err);
    }
  });

});


/** Service Functions
 * TODO: Better to move calculation/helper functions to seperate file.
 */



/** 
 * UI Partials
 * TOD: See if we can replace those by HB Dynamic Partials (Idea is to decouple UI Templating from Service Logic)
 */

// Create Reporters Data
export const createReporterTable = (reportersIssuesList) => {
  $('<table>').addClass('aui').append(
    $('<thead>').append(
      $('<tr>').append(
        $('<th>').text('Reporter'),
        $('<th>').text('Number of Requests')
      )
    ),
    $('<tbody>').append(
      $.map(reportersIssuesList, function (e) {
        // Map each request to a HTML table row
        return $('<tr>').append(
          $('<td>').text(e.name),
          $('<td>').text(e.requestCount),
        );
      })
    )
  ).appendTo('#main-content');
}

/**
 * IF No Requests Found
 */
const createNoIsssuesFound = () => {
  $('<div>').addClass('aui-message').append(
    $('<p>').addClass('title').append(
      $('<span>').addClass('aui-icon').addClass('icon-info'),
      $('<strong>').text("It looks like you don't have any requests!")
    )
  ).appendTo('#main-content');
}

/**
 * IF Error
 */
const createZeroNoIsssuesFound = (err) => {
  $('<div>').addClass('aui-message').addClass('aui-message-error').append(
    $('<p>').addClass('title').append(
      $('<span>').addClass('aui-icon').addClass('icon-error'),
      $('<strong>').text('An error occurred!')
    ),
    $('<p>').text(err.status + ' ' + err.statusText)
  ).appendTo('#main-content');
}